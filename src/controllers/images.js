import multer from 'multer';
import { Image } from '../models'

const upload = multer({storage: multer.memoryStorage()});

export const getImages = (req, res) => {
    Image.find().then(uris => {
        res.send({
            imageURI: uris, 
            statusCode: 200
        })
    })
}

export const postImages = [upload.single('image'), (req, res) => {
    Image.create(req.file.buffer).then(uri => {
        res.send({
            imageURI: uri,
            statusCode: 200
        })
    })
}];