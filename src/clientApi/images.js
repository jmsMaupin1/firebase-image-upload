import { getImageResponse, postImageResponse } from "../apiStubs";

// getImages :: () -> promise<getImagesResponse>
export const getImages = () => {
    console.log('getImages')
    return fetch('/api/images')
        .then(res => res.json())
        .then(json => {
            console.log(json)
            return json;
        })
}

// (formData) -> promise<postImageResponse>
export const postImages = (formData) => {
    return fetch('/api/images', {
        method: 'POST',
        body: formData
    })
    .then(res => res.json());
}