import fs from 'fs';
import admin from "firebase-admin";
const {Storage} = require('@google-cloud/storage');

let serviceAccount;

if(process.env.NODE_ENV === 'development') 
    serviceAccount = require('./firebaseAdminServiceAccount.json')
else {
    console.log(process.env.PRIVATE_KEY)
    serviceAccount = {
        projectId: JSON.parse(process.env.PROJECT_ID),
        clientEmail: JSON.parse(process.env.CLIENT_EMAIL),
        privateKey: JSON.parse(process.env.PRIVATE_KEY)
    }
}


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: 'kenziegram-bdff6.appspot.com'
})

let bucket = admin.storage().bucket();

const date = new Date();
const config = {
    action: 'read',
    expires: date.getMonth() + '-' + (date.getDay() + 1) + '-' + (date.getFullYear() + 1)
}


const find = () => {
    return bucket.getFiles()
        .then(files => Promise.all(files[0].map(file => file.getSignedUrl(config))))
        .then(urls => urls.map(url => url[0]))
        .catch(console.error)
}

const create = (image) => {
    const timestamp = Date.now();
    const file = bucket.file(timestamp.toString());
    return file.save(image).then(() => file.getSignedUrl(config))
        .then(url => url[0])
    
}

export default {
    find,
    create
}