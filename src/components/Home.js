import React, { useState, useEffect } from "react";
import { getImages, postImages } from '../clientApi';

export default function Home() {
  const [images, setImages] = useState([])
  const [uploadStatus, setUploadStatus] = useState(null)
  const [uploadPic, setUploadPic] = useState(null);

  const selectFile = files => {
    setUploadPic(files.target.files[0])
  }

  const uploadPicture = event => {
    event.preventDefault();

    const formData = new FormData();

    if(uploadPic) {
      formData.append('image', uploadPic)
      
      postImages(formData).then(json => {
        if(json.statusCode===200) {
          let listOfImages = images;
          listOfImages.push(json.imageURI);

          setImages(listOfImages);
          setUploadStatus(json.statusCode);
          
          setTimeout(() => {
            setUploadStatus(null);
          }, 1000)
        } else {
          setUploadStatus(json.statusCode);
        }
      })
    }
  }

  useEffect(() => {
    getImages()
      .then(img => setImages(img.imageURI))
  }, [])

  return (
    <>
      <form onSubmit={uploadPicture}>
        <input type="file" onChange={selectFile}/>
        <button type='submit'>Upload Photo</button>
      </form>
      {
        uploadStatus &&
        <p style={uploadStatus === 200 ? styles.success : styles.error}>
          {
            uploadStatus === 200 ? "Upload Successful" : "Upload Failed"
          }
        </p>
      }
      {images.map(img => <img key={img} style={styles.img} alt="" src={img}/>)}
    </>
  )
}

const styles = {
  img: {
    width: 300,
    height: 300
  },

  success: {
    color: 'blue'
  }, 

  error: {
    color: 'red'
  }
}